<?php

/**
 * @const FORUM Название папки модуля.
 */
define('FORUM', '/forum');

/**
 * @const ICONS Ширина и высота иконок.
 */
define('ICONS', 'style = "width: 22px; height: 22px"');
if (@$set['set_show_forum_nw_coll'] == null)
$set['set_show_forum_nw_coll'] = 0;

$k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_themes`'), 0);
$k_page = k_page($k_post, $set['p_str']);
$page = page($k_page);
$start = $set['p_str']*$page-$set['p_str'];
if ($k_post != 0) {
	?>
	<div class="act_them"><?=lang('Последние темы');?></div>
	<table class="act_them_fon">
        <?
        $themes = mysql_query('SELECT * FROM `forum_themes` ORDER BY `time_post` DESC LIMIT '.$set['set_show_forum_nw_coll']);
        while ($theme = mysql_fetch_object($themes)) {
            $creater = mysql_fetch_object(mysql_query('SELECT `id`, `nick`,`mylink` FROM `user` WHERE `id` = '.$theme->id_user));
            $count_posts = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id), 0);
            $hide = (user_access('forum_post_ed')) ? NULL : '`hide` = "0" AND';
            $last_post = mysql_fetch_object(mysql_query('SELECT `id`, `hide`, `id_user`, `time` FROM `forum_posts` WHERE '.$hide.' `id_theme` = '.$theme->id.' ORDER BY `id` DESC'));
            
            $who = ($last_post && $last_post->id_user != 0) ? mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$last_post->id_user)) : array();
            $who_id = ($who && $last_post && $last_post->id_user != 0) ? $who->id : 0;
            $who_nick = ($who && $last_post && $last_post->id_user != 0) ? $who->nick : $set['nick_system'];
            
            if ($theme->reason_close != NULL) {
                $type = '_close';
            } elseif ($theme->type == 1) {
                $type = '_up';
            } else {
                $type = NULL;
            }
            ?>
            <tr>
                <td class="icon14">
                    <img src="<?= FORUM ?>/icons/theme<?= $type ?>.png" alt="*" <?= ICONS ?> />
                </td>
                <td class="p_t">
                    <a href="<?= FORUM.'/'.$theme->id_forum.'/'.$theme->id_razdel.'/'.$theme->id ?>.html"><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a> (<?= $count_posts ?>)
                </td>
            </tr>
            <tr>
                <?
                if ($last_post) {
                    ?>
                    <td class="p_m" colspan="2">
                        <b><?=lang('Автор');?>: <a href="/id<?= $theme->id_user ?>"><?= $creater->nick ?></a> | <?=lang('Посл.');?>:</b> <a href="/id<?= $who_id ?>"><?= $who_nick ?></a> (<?= vremja($last_post->time) ?>)
                    </td>
                    <?
                } else {
                    ?>
                    <td class="p_m" colspan="2">
                        <b><?=lang('Автор');?>: <a href="/id<?= $theme->id_user ?>"><?= $creater->nick ?></a></b>
                    </td>
                    <?
                }
                ?>
            </tr>
            <?
        }
        ?>
    </table>
    <?
}

?>