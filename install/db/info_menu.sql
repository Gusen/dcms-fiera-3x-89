-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:37
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `info_menu`
--

CREATE TABLE IF NOT EXISTS `info_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(99) NOT NULL,
  `file` varchar(99) NOT NULL,
  `info` varchar(512) NOT NULL,
  `type` varchar(99) NOT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Профиль пользователя';

--
-- Дамп данных таблицы `info_menu`
--

INSERT DELAYED INTO `info_menu` (`id`, `name`, `file`, `info`, `type`, `pos`, `time`) VALUES
(1, 'Статистика', 'stat.php', 'test test', 'inc', 6, 111111111),
(2, 'Аватар', 'avatar.php', 'test test', 'inc', 1, 111111111),
(3, 'Статус', 'status.php', 'test test', 'inc', 3, 111111111),
(4, 'Пользовательская панель', 'user_panel.php', '', 'inc', -1, 111111111),
(10, 'Информация о пользователе ', 'info_ank.php', '', 'inc', 4, 0),
(11, 'Возможные ники', 'user_collisions.php', '', 'inc', 8, 0),
(9, 'Нижнее админ меню', 'foot.php', '', 'inc', 7, 0),
(12, 'Действия пользователя', 'user_d.php', '', 'inc', 9, 0),
(13, 'Статус бана в анкете', 'user_ban_status.php', 'панель статуса бана ', 'inc', 2, 0),
(14, 'Жалобы на анкеты', 'complaint_profile.php', '.....', 'inc', 0, 111111111);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `info_menu`
--
ALTER TABLE `info_menu`
  ADD PRIMARY KEY (`id`), ADD KEY `pos` (`pos`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `info_menu`
--
ALTER TABLE `info_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
